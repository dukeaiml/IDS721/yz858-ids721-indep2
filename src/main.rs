use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
// use std::fs::File;
// use std::io::Read;
// use base64::{encode};

#[get("/")]
async fn hello() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic Recycling Hub</title>
        </head>
        <body>
            <h1>Hi! Weclome to Plastic Recycling Hub.</h1>
            <p>
                This is where you can quick look up which type of plastic is recyclable. <br>
                There will be a plastic recyling symbols on each plastic item. <br>
                Please put the number in url to look up that plastic e.g. use http://127.0.0.1:8080/1 to look up plastic #1. <br>
                There are 7 different types of plastic. (Number 1 - 7)
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/1")]
async fn plastic1() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 1 PETE/PET</title>
        </head>
        <body>
            <h1>NUMBER 1 Polyethylene Terephthalate (PETE or PET)</h1>
            <p>
            Water bottles and plastic soda bottles are the most common containers made out of PET. It’s OK to recycle. <br>
            However, avoid reusing plastic containers made of PET. Why? PET is meant for single-use applications; 
            repeated use increases the risk of leaching and bacterial growth. Plus, it’s hard to clean or remove harmful chemicals. 
            PET may leach carcinogens.
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/2")]
async fn plastic2() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 2 HDPE</title>
        </head>
        <body>
            <h1>NUMBER 2 High-Density Polyethylene (HDPE)</h1>
            <p>
            Most milk jugs, detergent containers, and oil bottles are made from HDPE. 
            It’s a very common plastic and one of the safest to use. It’s also fully recyclable.
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/3")]
async fn plastic3() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 3 PVC</title>
        </head>
        <body>
            <h1>NUMBER 3 Polyvinyl Chloride (PVC)</h1>
            <p>
            PVC is used for plastic food wrapping because it’s soft and flexible. 
            Most consumer recyclers will not take PVC products. 
            Also, avoid reusing PVC products, especially when it comes to food or for children’s use. 
            They contain toxins that leach throughout their entire life cycle.
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/4")]
async fn plastic4() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 4 LDPE</title>
        </head>
        <body>
            <h1>NUMBER 4 Low-Density Polyethylene (LDPE)</h1>
            <p>
            LDPE is usually what plastic bags are made from. 
            You’ll also find LDPE in shrink wraps, dry cleaner garment bags, and other items.
            <br><br>  
            Though most plastic bags are not recyclable, 
            some companies and recycling centers have found alternatives or are investigating how to recycle plastic bags, 
            given their harmfulness to the environment.
            <br><br>    
            LDPE is reusable and safe to repurpose.
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/5")]
async fn plastic5() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 5 PP</title>
        </head>
        <body>
            <h1>NUMBER 5 Polypropylene (PP)</h1>
            <p>
            Polypropylene plastic is used in margarine and yogurt containers, potato chip bags, cereal bags, and more. 
            <br><br>
            Polypropylene is recyclable, although many recyclers still don’t accept it. PP is considered safe for reuse.
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/6")]
async fn plastic6() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 6 PS</title>
        </head>
        <body>
            <h1>NUMBER 6 Polystyrene (PS)</h1>
            <p>
            Avoid polystyrene as best as possible. It’s used for disposable styrofoam drinking cups, 
            take-out containers, packing peanuts, and more.
            <br><br>
            Polystyrene is not generally recyclable and accounts for about 35% of US landfill material. 
            Because it breaks apart so easily, it’s often found inside marine animals’ stomachs and littering our beaches.
            <br><br>
            Avoid reusing polystyrene. 
            Polystyrene’s chemical compounds have been linked with human health and reproductive system dysfunction. 
            Polystyrene may leach styrene, a possible human carcinogen, into food products (especially when heated in a microwave).
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[get("/7")]
async fn plastic7() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Plastic 7 OTHER</title>
        </head>
        <body>
            <h1>NUMBER 7 Polycarbonate, BPA, and Other (OTHER)</h1>
            <p>
            Assume that nothing with the #7 number can be recycled or reused. BPA can leak chemicals. 
            It’s an xenoestrogen, a known endocrine disruptor.
            </p>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .service(plastic1)
            .service(plastic2)
            .service(plastic3)
            .service(plastic4)
            .service(plastic5)
            .service(plastic6)
            .service(plastic7)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}