# yz858-ids721-indep7

I used actix to build a rust web service that tells people whether a specific plastic type is recyclable.

## Requirements
- [x] Simple Rest API/web service in Rust
- [x] Dockerfile to containerize service
- [x] CI/CD pipeline files

## Screenshots
This is the home page of the website that contains the instruction to how the service works.
![Home Pages](screenshots/index.png)
<br><br><br>
This is the sample screenshots for API for plastic #1 and #7 for demonstration.
![Plastic 1](screenshots/no1.png)
![Plastic 7](screenshots/no7.png)
<br><br><br>
This is to show that CI/CD pipeline successfully working.
![CICD](screenshots/CICD.png)